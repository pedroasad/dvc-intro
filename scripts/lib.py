import numpy as np


def load_features_and_labels(features_file):
    data = np.load(features_file)
    n = len(data)
    labels = np.hstack((
        np.zeros(n // 2),
        np.ones(n // 2),
    ))
    return data, labels
