import json

import click
import numpy as np
import pandas as pd
from scipy.stats import hmean
from tensorflow.python.keras import metrics, Sequential
from tensorflow.python.keras.layers import Flatten, Dense, Dropout
from tensorflow.random import set_seed

from scripts.config import get_params
from scripts.lib import load_features_and_labels


@click.command()
@click.option("--model-path", "-m", type=click.Path(writable=True, dir_okay=False), required=True)
@click.option("--train-features-file", "-t", type=click.Path(readable=True, dir_okay=False), required=True)
@click.option("--validate-features-file", "-v", type=click.Path(readable=True, dir_okay=False), required=True)
def main(model_path: str, train_features_file: str, validate_features_file: str):

    params = get_params()
    epochs = params.train.epochs
    batch_size = params.train.batch_size

    set_seed(params.train.random_seed)

    train_data, train_labels = load_features_and_labels(train_features_file)
    validate_data, validate_labels = load_features_and_labels(validate_features_file)

    history, model = train_model(batch_size, epochs, train_data, train_labels, validate_data, validate_labels)
    model.save(model_path)

    history_df = pd.DataFrame({
        "epoch": np.arange(epochs),
        "loss": history.history["loss"],
        "precision": history.history["precision"],
        "recall": history.history["recall"],
        "accuracy": history.history["accuracy"],
        "f1-score": hmean([
            history.history["precision"],
            history.history["recall"],
        ]),
    }).set_index("epoch")

    last_epoch = epochs - 1

    metrics_object = {
        "loss": history_df.loc[last_epoch, "loss"],
        "precision": history_df.loc[last_epoch, "precision"],
        "recall": history_df.loc[last_epoch, "recall"],
        "accuracy": history_df.loc[last_epoch, "accuracy"],
        "f1-score": history_df.loc[last_epoch, "f1-score"],
    }

    history_df.to_csv("plots/train.csv")

    with open("metrics/train.json", "w") as metrics_file:
        json.dump(metrics_object, metrics_file, indent=2)


def train_model(batch_size, epochs, train_data, train_labels, validate_data, validate_labels):
    model = Sequential()
    model.add(Flatten(input_shape=train_data.shape[1:]))
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(
        optimizer='rmsprop', loss='binary_crossentropy',
        metrics=[
            metrics.Precision(),
            metrics.Recall(),
            metrics.Accuracy(),
        ]
    )
    history = model.fit(
        train_data, train_labels,
        epochs=epochs,
        batch_size=batch_size,
        validation_data=(validate_data, validate_labels),
    )
    return history, model


if __name__ == '__main__':
    main()
