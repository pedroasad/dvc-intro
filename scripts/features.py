import click
import numpy as np
from tensorflow.keras import applications
from tensorflow.python.keras.preprocessing.image import ImageDataGenerator
from tensorflow.random import set_seed

from .config import get_params


@click.command()
@click.option("--features-file", "-f", type=click.Path(writable=True, dir_okay=False), required=True)
@click.option("--images-dir", "-d", type=click.Path(readable=True, file_okay=False), required=True)
@click.option("--section", "-s", type=click.Choice(["test", "train", "validate"]), required=True)
def main(features_file: str, images_dir: str, section: str):

    params = get_params()

    no_samples = getattr(params.dataset, f"{section}_size")

    set_seed(params.features.random_seed)
    data_generator = ImageDataGenerator(rescale=1. / 255)
    model = applications.VGG16(include_top=False, weights='imagenet')

    bottleneck_features = generate_features(
        data_generator, model,
        images_dir, params.features.images.width, params.features.images.height,
        params.features.batch_size, no_samples
    )
    np.save(features_file, bottleneck_features)


def generate_features(
        data_generator: ImageDataGenerator, model: applications.VGG16,
        data_dir: str, image_width: int, image_height: int,
        batch_size: int, no_samples: int) -> np.ndarray:

    generator = data_generator.flow_from_directory(
        data_dir,
        target_size=(image_width, image_height),
        batch_size=batch_size,
        class_mode=None,
        shuffle=False)
    bottleneck_features = model.predict(generator, no_samples // batch_size)

    return bottleneck_features


if __name__ == "__main__":
    main()
