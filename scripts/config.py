import yaml
from pydantic import BaseModel


class Dataset(BaseModel):
    test_size: int
    train_size: int
    validate_size: int


class Images(BaseModel):
    width: int
    height: int


class Features(BaseModel):
    batch_size: int
    images: Images
    random_seed: int


class Train(BaseModel):
    batch_size: int
    epochs: int
    random_seed: int


class Test(BaseModel):
    batch_size: int


class Params(BaseModel):
    dataset: Dataset
    features: Features
    train: Train
    test: Test


def get_params() -> Params:
    with open("params.yaml") as yaml_file:
        params = Params(**yaml.safe_load(yaml_file))
    return params
