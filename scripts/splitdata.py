import os, sys
print(os.getcwd())
print(sys.path)

import re
import shutil
from pathlib import Path

import click
from tqdm import tqdm

from scripts.config import get_params


@click.command()
@click.option("--dataset-dir", "-d", type=click.Path(readable=True, file_okay=False))
def main(dataset_dir: str):
    dataset_dir = Path(dataset_dir)

    params = get_params()

    train_dir = dataset_dir.parent / "train"
    validate_dir = dataset_dir.parent / "validate"
    test_dir = dataset_dir.parent / "test"

    sections = [
        (params.dataset.test_size, test_dir),
        (params.dataset.train_size, train_dir),
        (params.dataset.validate_size, validate_dir),
    ]

    cat_files = iter(sorted((dataset_dir / "cats").glob("*.jpg"), key=lambda path: int(re.match(r"cat\.(\d+)", path.stem).groups()[0])))
    dog_files = iter(sorted((dataset_dir / "dogs").glob("*.jpg"), key=lambda path: int(re.match(r"dog\.(\d+)", path.stem).groups()[0])))

    for section_size, section_dir in tqdm(sections):
        section_dir.mkdir(parents=True, exist_ok=True)
        (section_dir / "cats").mkdir(exist_ok=True)
        (section_dir / "dogs").mkdir(exist_ok=True)

        for _ in tqdm(range(section_size)):
            cat_file = next(cat_files)
            dog_file = next(dog_files)

            shutil.copy(cat_file, section_dir / cat_file.relative_to(dataset_dir))
            shutil.copy(dog_file, section_dir / dog_file.relative_to(dataset_dir))


if __name__ == "__main__":
    main()
