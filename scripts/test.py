import json

import click
from scipy.stats import hmean
from tensorflow.python import keras

from scripts.config import get_params
from scripts.lib import load_features_and_labels


@click.command()
@click.option("--model-path", "-m", type=click.Path(readable=True, dir_okay=False), required=True)
@click.option("--test-features-file", "-e", type=click.Path(readable=True, dir_okay=False), required=True)
def main(model_path: str, test_features_file: str):
    params = get_params()
    model = keras.models.load_model(model_path)

    evaluate_features, evaluate_labels = load_features_and_labels(test_features_file)

    results = model.evaluate(evaluate_features, evaluate_labels, batch_size=params.test.batch_size)
    metrics_object = dict(zip(model.metrics_names, results))

    metrics_object["f1-score"] = hmean([metrics_object["recall"], metrics_object["precision"]])

    with open("metrics/test.json", "w") as metrics_file:
        json.dump(metrics_object, metrics_file, indent=2)


if __name__ == "__main__":
    main()
