.. dvc-intro documentation master file, created by
   sphinx-quickstart on Wed Nov 18 15:39:08 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to dvc-intro's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. contents::

Command-line interface
======================

.. click:: scripts.features:main
   :prog: python -m scripts.features

.. click:: scripts.splitdata:main
   :prog: python -m scripts.splitdata

.. click:: scripts.test:main
   :prog: python -m scripts.test

.. click:: scripts.train:main
   :prog: python -m scripts.train

API reference
=============

:mod:`scripts.config`
---------------------

.. automodule:: scripts.config
   :members:
   :undoc-members:

:mod:`scripts.features`
-----------------------

.. automodule:: scripts.features
   :members:
   :undoc-members:

:mod:`scripts.lib`
------------------

.. automodule:: scripts.lib
   :members:
   :undoc-members:

:mod:`scripts.splitdata`
------------------------

.. automodule:: scripts.splitdata
   :members:
   :undoc-members:

:mod:`scripts.test`
-------------------

.. automodule:: scripts.test
   :members:
   :undoc-members:

:mod:`scripts.train`
--------------------

.. automodule:: scripts.train
   :members:
   :undoc-members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
